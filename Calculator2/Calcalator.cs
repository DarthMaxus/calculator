﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calculator2
{
    public class Calculator
    {
        public static double Calculate(string input)
        {
            Stack<string> output = GetExpression(input);
            double result = Solution(output);
            return result;
        }

        private static bool IsOperator(char s)
        {
            if ("/*-+^m".Contains(s))
                return true;
            return false;
        }
        private static bool IsOperator(string s)
        {
            if ("/*-+^m".Contains(s))
                return true;
            return false;
        }
        private static Stack<string> GetExpression(string input)
        {
            Stack<string> output = new Stack<string>();
            Stack<string> stack = new Stack<string>();
            string permitted = "1234567890-+/*^()";
            int count = 0;
            bool minus = false;
            for (int i = 0; i < input.Length; i++)
            {
                string temp = "";
                if (!permitted.Contains(input[i]))
                    throw new ForbiddenException("Выражение содержит недопустимые символы");
                if (input[i] == '-' && ((i > 0 && !Char.IsDigit(input[i - 1]) && input[i - 1] != ')') || i == 0) && input[i + 1] != '(')
                {
                    if (i > 0 && input[i] == '-' && (input[i - 1] == '/' || input[i - 1] == '*'))
                        throw new LogicException("Выражение нелогично");
                    i++;
                    temp += "-";
                }
                if (input[i] == '+' && ((i > 0 && !Char.IsDigit(input[i - 1]) && input[i - 1] != ')') || i == 0) && input[i + 1] != '(')
                {
                    if (i > 0 && input[i] == '+' && (input[i - 1] == '/' || input[i - 1] == '*'))
                        throw new LogicException("Выражение нелогично");
                    i++;
                    temp += "+";
                }
                if (Char.IsDigit(input[i]))
                {
                    while (i < input.Length && Char.IsDigit(input[i]))
                        temp += input[i++].ToString();
                    i--;
                    output.Push(temp);
                }
                if (input[i] == '+' || input[i] == '-')
                {
                    if (i == 0)
                        continue;
                    m:
                    if (i + 2 < input.Length)
                    {
                        if (IsOperator(input[i + 1]) && IsOperator(input[i + 2]))
                            throw new LogicException("Выражение содержит нелогичные операции");
                    }
                    if (stack.Count!=0)
                    {
                        if (stack.Peek() == "(")
                        {
                            stack.Push(input[i].ToString());
                        }
                        else
                        {
                            output.Push(stack.Pop());
                            goto m;
                        }
                    }
                    else
                    {
                        stack.Push(input[i].ToString());
                    }
                }
                if (input[i] == '*' || input[i] == '/')
                {
                    if (i == 0)
                        continue;
                    if (i + 1 < input.Length)
                    {
                        if (IsOperator(input[i + 1]))
                            throw new LogicException("Выражение содержит нелогичные операции");
                    }
                    if (stack.Count != 0)
                    {
                        if (stack.Peek() != "*" && stack.Peek() != "/")
                        {
                            stack.Push(input[i].ToString());
                        }
                        else
                        {
                            output.Push(stack.Pop());
                        }
                    }
                    else
                    {
                        stack.Push(input[i].ToString());
                    }
                }
                if (input[i] == '(')
                {
                    if (i > 0 && input[i - 1] == ')')
                        throw new PriorityException("Выражение нелогично");
                    if (i + 1 < input.Length && input[i + 1] == ')')
                        throw new PriorityException("Выражение имеет проблемы со скобками");
                    if (i > 0)
                    {
                        if (i > 1)
                        {
                            if (input[i - 1] == '-' && !Char.IsDigit(input[i - 2]))
                                minus = !minus;
                        }
                        else if (input[i - 1] == '-')
                            minus = !minus;
                    }
                    count++;
                    stack.Push(input[i].ToString());
                }
                if (input[i] == ')')
                {
                    if (count == 0)
                        throw new PriorityException("В выражении содержится ошибка, связанная со скобками");
                    count--;
                    int index = stack.Count - 1;
                    Stack<string> _temp = new Stack<string>();
                    while (stack.Peek() != "(") // Просматриваем стек, пока не встретится скобка
                    {
                        index--;
                        _temp.Push(stack.Pop());
                    } // После цикла верхний элемент стека будет ( скобкой 
                    int temp_count = _temp.Count;
                    Stack<string> reverse = new Stack<string>();
                    for (int j = 0; j < temp_count; j++)
                        reverse.Push(_temp.Pop());
                    for (int j = 0; j < temp_count; j++)
                    {
                        output.Push(reverse.Pop());
                    }
                    if (minus)
                    {
                        output.Push("m");
                        minus = false;
                    }
                    stack.Pop();
                }
                if (input[i] == '^')
                {
                    stack.Push(input[i].ToString());
                }
            }
            if (count != 0)
                throw new PriorityException("В выражении содержится ошибка, связанная со скобками");
            int stack_count = stack.Count;
            for (int i = 0; i < stack_count; i++)
                output.Push(stack.Pop());
            int output_count = output.Count;
            Stack<string> reversed_output = new Stack<string>();
            for (int i = 0; i < output_count; i++)
                reversed_output.Push(output.Pop());
            return reversed_output;
        }
        private static double Solution(Stack<string> input)
        {
            Stack<string> temp = new Stack<string>();
            Stack<string> last_two = new Stack<string>();
            string _temp;
            while (input.Count > 0)
            {
                if(IsOperator(input.Peek()))
                {
                    switch (input.Pop())
                    {
                        case "+":
                            last_two.Push(temp.Pop());
                            last_two.Push(temp.Pop());
                            _temp = (double.Parse(last_two.Pop()) + double.Parse(last_two.Pop())).ToString();
                            input.Push(_temp);
                            break;
                        case "-":
                            last_two.Push(temp.Pop());
                            last_two.Push(temp.Pop());
                            _temp = (double.Parse(last_two.Pop()) - double.Parse(last_two.Pop())).ToString();
                            input.Push(_temp);
                            break;
                        case "m":
                            _temp = (0 - double.Parse(temp.Pop())).ToString();
                            input.Push(_temp);
                            break;
                        case "*":
                            last_two.Push(temp.Pop());
                            last_two.Push(temp.Pop());
                            _temp = (double.Parse(last_two.Pop()) * double.Parse(last_two.Pop())).ToString();
                            input.Push(_temp);
                            break;
                        case "/":
                            last_two.Push(temp.Pop());
                            last_two.Push(temp.Pop());
                            double oper1 = double.Parse(last_two.Pop());
                            double oper2 = double.Parse(last_two.Pop());
                            if (oper2 == 0)
                                throw new DivisionByZeroException("Деление на ноль");
                            _temp = (oper1 / oper2).ToString();
                            input.Push(_temp);
                            break;
                        case "^":
                            last_two.Push(temp.Pop());
                            last_two.Push(temp.Pop());
                            if (double.Parse(last_two.First()) == 0 && double.Parse(last_two.Last()) == 0)
                                throw new PowZeroZeroException("Невозможно возвести 0 в степень 0");
                            _temp = (Math.Pow(double.Parse(last_two.Pop()), double.Parse(last_two.Pop()))).ToString();
                            input.Push(_temp);
                            break;
                    }
                }
                else
                {
                    temp.Push(input.Pop());
                }
            }
            return double.Parse(temp.Pop());
        }
    }

}
