﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calculator2
{
    public class CalculatorException: Exception
    {
        public CalculatorException(string message)
            : base(message)
        { }
    }
    public class LogicException : CalculatorException
    {
        public LogicException(string message)
            : base(message)
        { }
    }
    public class PriorityException: CalculatorException
    {
        public PriorityException(string message)
            : base(message)
        { }
    }
    public class DivisionByZeroException: CalculatorException
    {
        public DivisionByZeroException(string message)
            : base(message)
        { }
    }
    public class ForbiddenException: CalculatorException
    {
        public ForbiddenException(string message)
            :base(message)
        { }
    }
    public class PowZeroZeroException : CalculatorException
    {
        public PowZeroZeroException(string message)
            : base(message)
        { }
    }
}
