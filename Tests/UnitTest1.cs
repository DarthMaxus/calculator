﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using Calculator2;
namespace Tests
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void SimpleAdd()
        {
            string expression = "2+3";
            double expected = 5;
            Assert.AreEqual(Calculator.Calculate(expression), expected);
        }
        [TestMethod]
        public void PlusMinus()
        {
            string expression = "2+-6";
            double expected = -4;
            Assert.AreEqual(Calculator.Calculate(expression), expected);
        }
        [TestMethod]
        public void PriorityTest3()
        {
            string expression = "-(33*11)/(-11)";
            double expected = 33;
            Assert.AreEqual(Calculator.Calculate(expression), expected);
        }
        [TestMethod]
        public void Minus2()
        {
            string expression = "-2";
            double expected = -2;
            Assert.AreEqual(Calculator.Calculate(expression), expected);
        }
        [TestMethod]
        public void PriorityTest2()
        {
            string expression = "2+3*5";
            double expected = 17;
            Assert.AreEqual(Calculator.Calculate(expression), expected);
        }
        [TestMethod]
        public void MinusBeforePriority()
        {
            string expression = "-(+2-1)";
            double expected = -1;
            Assert.AreEqual(Calculator.Calculate(expression), expected);
        }
        [TestMethod]
        public void PriorityTest()
        {
            string expression = "2+3*(3+3)";
            double expected = 20;
            Assert.AreEqual(Calculator.Calculate(expression), expected);
        }
        [TestMethod]
        public void DivisionOfZero()
        {
            string expression = "2+0/(2+2)-0-1+0";
            double expected = 1;
            Assert.AreEqual(Calculator.Calculate(expression), expected);
        }
        [TestMethod]
        public void PowZero()
        {
            string expression = "5^0";
            double expected = 1;
            Assert.AreEqual(Calculator.Calculate(expression), expected);
        }
        [TestMethod]
        public void PowOne()
        {
            string expression = "314^1";
            double expected = 314;
            Assert.AreEqual(Calculator.Calculate(expression), expected);
        }
        [TestMethod]
        public void TimeTest100kGet()
        {
            string expression = "";
            for (int i = 0; i < 50000; i++)
            {
                expression += "+2";
            }
            for (int i = 0; i < 50000; i++)
            {
                expression += "-2";
            }
            double expected = 0;
            Assert.AreEqual(Calculator.Calculate(expression), expected);
        }
        [TestMethod]
        public void DoublePriority()
        {
            string expression = "6*2-3*(20/5)";
            double expected = 0;
            Assert.AreEqual(Calculator.Calculate(expression), expected);
        }
        [TestMethod]
        public void DivisionByZero()
        {
            string expression = "4/(2-2)";
            Assert.ThrowsException<DivisionByZeroException>(() => Calculator.Calculate(expression));
        }
        [TestMethod]
        public void IncorrectInput()
        {
            string expression = "2+3c";
            Assert.ThrowsException<ForbiddenException>(() => Calculator.Calculate(expression));
        }
        [TestMethod]
        public void PriorityMistake()
        {
            string expression = "((3+1)";
            Assert.ThrowsException<PriorityException>(() => Calculator.Calculate(expression));
        }
        [TestMethod]
        public void MultiplyByZero()
        {
            string expression = "(2+3*5-6)*(2-2)";
            double expected = 0;
            Assert.AreEqual(Calculator.Calculate(expression), expected);
        }
        [TestMethod]
        public void PowZeroZero()
        {
            string expression = "0^0";
            Assert.ThrowsException<PowZeroZeroException>(() => Calculator.Calculate(expression));
        }
        [TestMethod]
        public void LogicMistake1()
        {
            string expression = "(2+3)(2+3)";
            Assert.ThrowsException<PriorityException>(() => Calculator.Calculate(expression));
        }
        [TestMethod]
        public void LogicMistake2()
        {
            string expression = "((2+1)";
            Assert.ThrowsException<PriorityException>(() => Calculator.Calculate(expression));
        }
        [TestMethod]
        public void LogicMistake3()
        {
            string expression = "(2+1)+()";
            Assert.ThrowsException<PriorityException>(() => Calculator.Calculate(expression));
        }
        [TestMethod]
        public void LogicMistake4()
        {
            string expression = "(2+1)+++3";
            Assert.ThrowsException<LogicException>(() => Calculator.Calculate(expression));
        }
        [TestMethod]
        public void BigNumbers()
        {
            string expression = "1234567890126/1234567890126+(1234567890126-1234567890126)*(1234567890126/1234567890126)";
            double expected = 1;
            Assert.AreEqual(Calculator.Calculate(expression), expected);
        }
    }
}
