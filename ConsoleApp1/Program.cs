﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Calculator2;

namespace ConsoleApp1
{
    class Program // надо сделать: покрытие кода тестами
    {
        static void Main(string[] args)
        {
            while (true)
            {
                try
                {
                    Console.Write("Введите выражение: ");
                    string input = Console.ReadLine();
                    double result = Calculator.Calculate(input);
                    Console.WriteLine("Ответ: " + result.ToString());
                }
                catch (CalculatorException ex)
                {
                    Console.WriteLine($"Ошибка: {ex.Message}");
                }
            }
        }
    }
}
